import helpers, random, constant

class Career:
    
    StartCareer:int = 0
    Increase:float = 0
    rnd = random.Random()

    def __init__(self):
        self.years = []

    def SetupSimpleCareer(self, shb, nyear):
        newShb = shb
        for _ in range(0, nyear):
            self.years.append(YearCareer(newShb, constant.Nb_Hour_Year))
            newShb += newShb * self.Increase
    
    def SetupRandomCareer(self, salary_brut_by_hour, change_hour_percent):        
        # start = 43
        # if self.StartCareer + start < 62:
        #     start = 62 - self.StartCareer
        # end = 67 - self.StartCareer
        # if start >= end:
        #     end = start + 5
        nyear = 68 - self.StartCareer
        newShb = salary_brut_by_hour
        hour = 0
        for _ in range(0, nyear):
            increase = self.rnd.randint(0, 2) / 100.0
            if self.rnd.randint(0, 100) < change_hour_percent:
                hour = self.rnd.randint(600, constant.Nb_Hour_Year)
            if hour == 0:
                self.StartCareer += 1
            else:
                self.years.append(YearCareer(newShb, hour))
            newShb += newShb * increase
    
    def AgeEnd(self, nbyears:int=0):
        if nbyears == len(self.years):
            nbyears = len(self.years)
        return self.StartCareer + nbyears

    def CotisationPoint(self, nbyears:int=0):
        points = 0
        if nbyears > len(self.years): 
            nbyears = len(self.years)
        for id in range(0, nbyears):
            y = self.years[id]
            points += y.CotisationPoint()
        return points
    
    def TimeToWork(self, nbyears:int=0):
        if nbyears == len(self.years):
            nbyears = len(self.years)
        years = self.years[:nbyears+1]
        hours = [y.NbHour for y in years]
        return sum(hours)
    
    def PensionNewSystem(self, nb_enfant, nbyears:int=0):
        if nbyears > len(self.years):
            nbyears = len(self.years)
        ct = self.CotisationPoint(nbyears)
        pension = (ct * constant.Val_Point_Sell) / 12
        age_depart = self.StartCareer + nbyears
        #TODO age_pivot vas changer, il faut donc que je prenne en compte cette donnée
        bm_resultat = ((age_depart - constant.Age_Pivot) * 0.05)
        pension = pension + (pension * bm_resultat)
        #TODO bonus different sur le 3eme enfant
        pension = pension + (pension * nb_enfant * 0.05)
        if nbyears >= 43:
            # Retraite min 85% si carriere complete
            smic = helpers.SMIC_at_end(nbyears)
            minRetraite = smic * 0.85
            if pension < minRetraite:
                pension = minRetraite
        return pension

    def PensionCurrentSystem(self, nb_enfant, nbyears:int=0):
        if nbyears > len(self.years):
            nbyears = len(self.years)
        ancien_dureecotisation = (nb_enfant * 2) + nbyears
        ancien_systeme = self.AverageSalary25(nbyears) * 0.6
        if ancien_dureecotisation > 43:
            surcote = ((ancien_dureecotisation - 43)*4.0) * 0.0125
            ancien_systeme += ancien_systeme * surcote
        elif self.AgeEnd(nbyears) < 67 and ancien_dureecotisation < 43:
            decote = (43 - ancien_dureecotisation) * 0.05
            ancien_systeme -= ancien_systeme * decote

        if nb_enfant >= 3:
            ancien_systeme += ancien_systeme * 0.1
        return ancien_systeme

    def AverageSalary25(self, nbyears:int=0):
        if nbyears > len(self.years):
            nbyears = len(self.years)
        years = self.years[:nbyears+1] 
        sortedYears = sorted(years, key=lambda y: y.BrutSalary, reverse= True)
        sliceYears = sortedYears[:25]
        brutYears = [y.MensualSalary() for y in sliceYears]
        return sum(brutYears) / len(brutYears)
    
    def LastSalary(self):
        return self.years[-1].MensualSalary()

class YearCareer:    
    def __init__(self, shb, nbhour):
        self.BrutSalary = shb
        self.NbHour = nbhour

    def MensualSalary(self):
        return self.BrutSalary * constant.Nb_Hour_Month
    
    def AnnualSalary(self):
        return self.BrutSalary * self.NbHour

    def Contribution(self):
        # Cotisation à 25.31%
        brut = self.BrutSalary * self.NbHour
        if brut > 120000:
            brut = 120000
        return brut * 0.2531

    def CotisationPoint(self):
        return int(self.Contribution() / constant.Val_Point_Buy)

if __name__ == '__main__':
    print("it's lib")