import argparse
import helpers, carriere, dump, constant


# Salaire mensuel brut
smb = constant.Smic_Hour_Brut * constant.Nb_Hour_Month
# % Cotisation
a = 0.2531
# Augmentation par an
aug = constant.Aug_Base
# Valeur du bonus/malus par an
bm = 0.05
# Nombre d'enfant
ne = 0
# Pourcentage par enfant
pe = 0.05

parser = argparse.ArgumentParser()
simpe_gr = parser.add_argument_group('simple')
simpe_gr.add_argument('--augmentation')
simpe_gr.add_argument('--debut')
simpe_gr.add_argument('--enfants')
aleatoire_gr = parser.add_argument_group('aleatoire')
aleatoire_gr.add_argument('--random', action='store_true')
parser.add_argument('--age', type=int, default=25)
parser.add_argument('--salaire', type=float, default=1539.45)
args = parser.parse_args()

if args.salaire is not None:
    smb = float(args.salaire)
else:
    smb = helpers.chooseValue("Ton salaire brut mensuel ?", smb)

if not args.random:
    if args.augmentation is not None:
        aug = float(args.augmentation)
    else:
        aug = helpers.chooseValue("Augmentation par an ?", aug)

    if args.enfants is not None:
        ne = int(args.enfants)
    else:
        ne = int(helpers.chooseValue("Nombre d'enfants ?", ne))

age = args.age

shb = smb / constant.Nb_Hour_Month
na = 68 - age

#Creation de carrière
career = carriere.Career()
career.StartCareer = age
if not args.random:
    career.Increase = aug
    
    career.SetupSimpleCareer(shb, na)
else:
    career.SetupRandomCareer(shb, 10)

dumpInfos = dump.Dump(career)
dumpInfos.dumpCareer()
dumpInfos.Save()