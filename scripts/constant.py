# Nombre d'heure de travail par mois
Nb_Hour_Month = 151.67
# Nombre d'heure de travail par an
Nb_Hour_Year = 1607
# Salaire horaire brut
Smic_Hour_Brut = 10.15  # SMIC 2020
# Age pivot (est réévaluable)
Age_Pivot = 64
# Augmentation de base par an
Aug_Base = 0.01
# Valeur du point pour l'achat de retrait
Val_Point_Sell = 0.55
# Valeur du point de cotisation
Val_Point_Buy = 10