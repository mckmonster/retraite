import json, carriere, helpers, constant


class Dump:
    dump = {
        "StartAge": 0,
        "StartSalary": 0,
        "CotisationTime": 0,
        "BigGraph": [],
        "LittleGraph": []
    }  # type: dict

    def __init__(self, career: carriere.Career):
        self.Career = career
        self.dump["StartAge"] = career.StartCareer
        self.dump["StartSalary"] = career.years[0].MensualSalary()
        self.dump["CotisationTime"] = "{} - {}".format(len(career.years) - 5, len(career.years))

    def dumpCareer(self):
        self._addCareer()
        self._addCotisation()
        self._addPension()

    def _addCareer(self):
        dataCareer = []
        dataPoints = []
        dataHours = []
        career = self.Career

        for index, y in enumerate(career.years):
            age = index + career.StartCareer
            dataCareer.append(
                {
                    "x": age,
                    "y": "{:.2f}".format(y.MensualSalary())
                })
            dataPoints.append({
                "x": age,
                "y": "{:.2f}".format(y.CotisationPoint())
            })
            dataHours.append({
                "x": age,
                "y": y.NbHour
            })

        data = {
            "type": "line",
            "data": {
                "datasets": [
                    {
                        "data": dataCareer,
                        "label": "Salaire Brut",
                        "borderColor": "#3e95cd",
                        "fill": False,
                        "lineTension": 0
                    },
                    {
                        "data": dataPoints,
                        "label": "Points cotisés",
                        "borderColor": "#ff0000",
                        "fill": False,
                        "lineTension": 0.25
                    },
                    {
                        "data": dataHours,
                        "label": "Nombre d'heure dans l'année",
                        "borderColor": "green",
                        "fill": False,
                        "lineTension": 0.25
                    }
                ]
            },
            "options": {
                "title": {
                    "display": True,
                    "text": "Carrière"
                },
                "scales": {
                    "xAxes": [{
                        "type": "linear"
                    }]
                }
            }
        }
        self.dump["BigGraph"].append(data)

    def _addCotisation(self):
        career = self.Career
        dataCumulPoints = []

        for index, _ in enumerate(career.years):
            age = index + career.StartCareer
            dataCumulPoints.append(
                {
                    "x": age,
                    "y": career.CotisationPoint(index)
                })

        data = {
            "type": "line",
            "data": {
                "datasets": [
                    {
                        "data": dataCumulPoints,
                        "label": "Cumul Points",
                        "borderColor": "#aa0000",
                        "fill": False,
                        "lineTension": 0
                    }
                ]
            },
            "options": {
                "title": {
                    "display": True,
                    "text": "Cotisations"
                },
                "scales": {
                    "xAxes": [{
                        "type": "linear"
                    }]
                }
            }
        }
        self.dump["LittleGraph"].append(data)

    def _addPension(self):
        career = self.Career
        dataPensionNS = []
        dataPensionCS = []
        dataSMIC = []

        for index, _ in enumerate(career.years):
            age = index + career.StartCareer
            if age >= 62:
                dataPensionNS.append(
                    {
                        "x": age,
                        "y": "{:.2f}".format(career.PensionNewSystem(0, index))
                    })
                dataPensionCS.append(
                    {
                        "x": age,
                        "y": "{:.2f}".format(career.PensionCurrentSystem(0, index))
                    })
                dataSMIC.append({
                    "x": age,
                    "y": helpers.SMIC_at_end(index)
                })

        data = {
            "type": "line",
            "data": {
                "datasets": [
                    {
                        "data": dataPensionNS,
                        "label": "Pension Nouveau Système",
                        "borderColor": "#00aa00",
                        "fill": False,
                        "lineTension": 0
                    },
                    {
                        "data": dataPensionCS,
                        "label": "Pension Système Actuel",
                        "borderColor": "#0000aa",
                        "fill": False,
                        "lineTension": 0
                    },
                    {
                        "data": dataSMIC,
                        "label": "SMIC",
                        "borderColor": "red",
                        "fill": False,
                        "lineTension": 0
                    }
                ]
            },
            "options": {
                "title": {
                    "display": True,
                    "text": "Pension"
                },
                "scales": {
                    "xAxes": [{
                        "type": "linear"
                    }]
                }
            }
        }
        self.dump["LittleGraph"].append(data)

    def Save(self):
        filename = "data_{}_{:.0f}_{}.json".format(self.Career.StartCareer, self.Career.years[0].MensualSalary(), self.Career.TimeToWork(len(self.Career.years)))
        with open("site/"+filename, 'w') as f:
            json.dump(self.dump, f, indent=4)

        filelist = None
        with open('site/data_list.json', 'r') as f:
            filelist = json.load(f)
            filelist.append({
                "start_career": self.Career.StartCareer,
                "start_salary": self.Career.years[0].MensualSalary(),
                "time_worked": self.Career.TimeToWork(len(self.Career.years)),     
                "file": filename
            })
        with open('site/data_list.json', 'w') as f:
            json.dump(filelist, f, indent=4)