# retraite
Le code est fait à partir de ce que j'ai compris de ça  
https://droit-finances.commentcamarche.com/download/telecharger-470-reforme-des-retraites-2020-texte-du-projet-de-loi

# Etude de cas sur le site du gouv
## Aujourd’hui, un mode de calcul complexe dépendant de nombreux facteurs
https://reforme-retraite.gouv.fr/le-saviez-vous/les-infographies/le-systeme-actuel/article/aujourd-hui-un-mode-de-calcul-complexe-dependant-de-nombreux-facteurs

### resultat
Retraite par mois : 1148.58€  
Bonus/malus : -10.0%

Donc **400€ de moins** qu'avec le système actuel

# Usage
* https://www.w3schools.com/w3css
* https://www.chartjs.org/